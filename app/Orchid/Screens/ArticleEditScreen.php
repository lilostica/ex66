<?php

namespace App\Orchid\Screens;

use App\Article;
use App\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class ArticleEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create Article';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Manage Article';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Article $article): array
    {
        $this->exists = $article->exists;
        if ($this->exists){
            $this->name = "Edit {$article->title} article";
        }
        return [
            'article' => $article
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create article')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('article.title')
                    ->title('Title')
                    ->placeholder('Enter article title')
                    ->required(),
                Relation::make('Author')
                    ->title('Author')
                    ->required()
                    ->fromModel(User::class,'name','id'),
                Quill::make('article.content')
                    ->title('Article content')
                    ->required()
            ])
        ];
    }
}
