<?php

namespace App\Orchid\Screens;

use App\Article;
use App\Orchid\Layouts\ArticleListLayout;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class ArticleListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'All Articles';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All Articles Description';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'articles' => Article::filters()->defaultSort('id')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new article')
                ->icon('icon-pencil')
                ->route('platform.articles.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            ArticleListLayout::class
        ];
    }

    /**
     * @param Article $article
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Article $article, Request $request)
    {
        $article->fill($request->get('article'))->save();
        Alert::info('You have successfuly created post');
        return redirect()->route('platform.articles.list');
    }

    /**
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Article $article)
    {
        $article->delete()
            ? Alert::info('Delete')
            :Alert::warning('No deleted');
        return redirect()->route('platform.articles.list');
    }
}
